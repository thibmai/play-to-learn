module.exports = function (eleventyConfig){
   
    eleventyConfig.addCollection("sortedByOrder", function (collectionApi) {
        return collectionApi.getAll().sort((a, b) => {
    
          if (a.data.order > b.data.order) return 1;
          else if (a.data.order < b.data.order) return -1;
          else return 0;
        });
      });

      eleventyConfig.addPassthroughCopy({ "static/css": "/css" });

      return{
        dir: {
            input: "src",
            output: "public",
            includes: "layouts",
        },
    };
};
